## Global variables

variable "global_tags" {
  description = "Global Tags"
  type        = map(string)
}

## Module Specific variables

variable "module_tags" {
  description = "Module Tags"
  default     = {}
}

variable "name" {
  description = "Name for all resources to start with"
}

## Resource variables

# Assume Iam Role

variable "create_assume_role" {
  description = "Set to false if you do not want to create Iam Assume Role Key for module"
  default     = "false"
}

variable "assume_role_assume_policy" {
  description = "Assume Role Assume Policy Document MAP variable that include statement effect and Principals"
  default     = []
}

variable "assume_role_policy" {
  description = "Assume Role Policy Document MAP variable that include statement effect and Principals"
  default     = []
}

variable "assume_role_policy_condition" {
  description = "Assume Role Policy Document MAP variable that include statement effect and Principals, and Condition as well"
  default     = []
}

variable "assume_role_tags" {
  description = "Custome Assume Role Tags"
  default     = {}
}

