output "assume_role_id" {
  description = "The ID of the Assume Role"
  value       = join("", aws_iam_role.assume_role.*.id)
}

output "assume_role_arn" {
  description = "The ARN of the Assume Role"
  value       = join("", aws_iam_role.assume_role.*.arn)
}

output "assume_role_name" {
  description = "The Name of the Assume Role"
  value       = join("", aws_iam_role.assume_role.*.name)
}

output "assume_role_instance_profile_id" {
  description = "The ID of the Assume Role Instance Profile"
  value = join(
    "",
    aws_iam_instance_profile.assume_role_instance_profile.*.id,
  )
}

output "assume_role_instance_profile_arn" {
  description = "The ARN of the Assume Role Instance Profile"
  value = join(
    "",
    aws_iam_instance_profile.assume_role_instance_profile.*.arn,
  )
}

output "assume_role_instance_profile_name" {
  description = "The Name of the Assume Role Instance Profile"
  value = join(
    "",
    aws_iam_instance_profile.assume_role_instance_profile.*.name,
  )
}

