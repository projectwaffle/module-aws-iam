# AWS IAM Terraform module

[![CircleCI](https://circleci.com/bb/projectwaffle/module-aws-iam.svg?style=svg)](https://circleci.com/bb/projectwaffle/module-aws-iam/)

Terraform module which creates IAM resources on AWS. Might be used for different modules

These types of resources are supported:

* [IAM Role](https://www.terraform.io/docs/providers/aws/r/iam_role.html)
* [IAM Instance Profile](https://www.terraform.io/docs/providers/aws/r/iam_instance_profile.html)
* [IAM Role Policy](https://www.terraform.io/docs/providers/aws/r/iam_role_policy.html)

Sponsored by [Draw.io - the best way to draw AWS diagrams](https://www.draw.io/)

## Usage

```hcl
module "ec2_assume_role" {
  source = "bitbucket.org/projectwaffle/module-aws-iam.git?ref=tags/v0.0.1"

  resource_identities = {
    assume_role = "ec2-role"
  }

  name               = "training-prod"
  create_assume_role = "true"
  global_tags        = "${var.global_tags}"

  assume_role_assume_policy = [
    {
      statement_efect                  = "Allow"
      statement_principals_type        = "Service"
      statement_principals_identifiers = "ec2.amazonaws.com"
    },
  ]

  assume_role_policy = [
    {
      statement_efect     = "Allow"
      statement_actions   = "ec2:*"
      statement_resources = "*"
    },
    {
      statement_efect     = "Allow"
      statement_actions   = "ssm:*"
      statement_resources = "*"
    },
  ]

  assume_role_policy_condition = [
    {
      statement_efect     = "Allow"
      statement_actions   = "iam:PassRole"
      statement_resources = "*"
      condition_test      = "StringEquals"
      condition_variable  = "am:PassedToService"
      condition_values    = "ec2.amazonaws.com,ec2.amazonaws.com.cn"
    },
  ]
}
```

## Resource Identity

In order to add custom names to resources, each module has two variables to be used for this.
* Variable called `name` - this will be used as the base name for each resource in the module
    * `name = "training-prod"` 
* MAP variable called `resource_identities` - used to append specific name to each resoruce
```hcl
  resource_identities = {
    kms_key   = "ct-key"
    kms_alias = "ct-alias"
    kms_grant = "ct-grant"
  }
```

Both variables should be declared in module section: 
```hcl
module "config" {
  ......
  name = "training-prod"
  resource_identities = {
    kms_key   = "ct-key"
    kms_alias = "ct-alias"
    kms_grant = "ct-grant"
  }
  ......
}  
```

Next is an example of variable used in the Module resoruce names or tgas 'Name':
```hcl
  name  = "${format("%s-%s", var.name, var.resource_identities["cloudtrail"])}"
  tags  = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["cloudtrail"],)), var.global_tags, var.module_tags, var.cloudtrail_tags)}"
```


## Resource Tagging

There are three level of resource tagging declaration
* Global level - declare tagging strategy that will be applied on all the AWS resources

```hcl
variable "global_tags" {
  type = "map"

  default = {
    environment     = "prod"
    role            = "infrastructure"
    version         = "0.1"
    owner           = "Sergiu Plotnicu"
    bu              = "IT"
    customer        = "project waffle"
    project         = "training"
    confidentiality = "open"
    compliance      = "N/A"
    encryption      = "disabled"
  }
}
```

* Module level - taggig that will affect only resources created by the module itself
```hcl
module "kms" {

  module_tags = {
    bu = "security"
  }
  global_tags         = "${var.global_tags}"
}
```

* Resource level - each resource can be tagged with custome tag or tage that will rewrite all the others one

```hcl
variable "kms_tags" {
  description = "Custome KMS Tags"
  default     = {
    role = "encryption"
  }
}
```

Combination of all this variables should cover all the tagging requirements. 
Tags are beeing merged so, resource level ones, will rewrite modules one, that will rewrite global ones.

## Terraform version

Terraform version 0.11.10 or newer is required for this module to work.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| assume\_role\_assume\_policy | Assume Role Assume Policy Document MAP variable that include statement effect and Principals | list | `[]` | no |
| assume\_role\_policy | Assume Role Policy Document MAP variable that include statement effect and Principals | list | `[]` | no |
| assume\_role\_policy\_condition | Assume Role Policy Document MAP variable that include statement effect and Principals, and Condition as well | list | `[]` | no |
| assume\_role\_tags | Custome Assume Role Tags | map | `{}` | no |
| create\_assume\_role | Set to false if you do not want to create Iam Assume Role Key for module | string | `false` | no |
| global\_tags | Global Tags | map(string) | - | yes |
| module\_tags | Module Tags | map | `{}` | no |
| name | Name for all resources to start with | string | - | yes |

## Outputs

| Name | Description |
|------|-------------|
| assume\_role\_arn | The ARN of the Assume Role |
| assume\_role\_id | The ID of the Assume Role |
| assume\_role\_instance\_profile\_arn | The ARN of the Assume Role Instance Profile |
| assume\_role\_instance\_profile\_id | The ID of the Assume Role Instance Profile |
| assume\_role\_instance\_profile\_name | The Name of the Assume Role Instance Profile |
| assume\_role\_name | The Name of the Assume Role |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->


## Authors

Module is maintained by [Sergiu Plotnicu](https://bitbucket.org/projectwaffle/)

## License

Licensed under Sergiu Plotnicu, please contact him at - sergiu.plotnicu@gmail.com


