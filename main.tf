# Resource Iam Assume Role

data "aws_iam_policy_document" "assume_role_assume_policy" {
  count = var.create_assume_role ? 1 : 0

  statement {
    effect = var.assume_role_assume_policy[count.index]["statement_efect"]

    actions = ["sts:AssumeRole"]

    principals {
      type = var.assume_role_assume_policy[count.index]["statement_principals_type"]
      identifiers = split(
        ",",
        var.assume_role_assume_policy[count.index]["statement_principals_identifiers"],
      )
    }
  }
}

resource "aws_iam_role" "assume_role" {
  count = var.create_assume_role ? 1 : 0

  name_prefix = format("%s-", var.name)
  assume_role_policy = element(
    data.aws_iam_policy_document.assume_role_assume_policy.*.json,
    count.index,
  )

  tags = merge(
    {
      "Name" = format("%s-", var.name)
    },
    var.global_tags,
    var.module_tags,
    var.assume_role_tags,
  )
}

data "aws_iam_policy_document" "assume_role_policy" {
  count = var.create_assume_role ? length(var.assume_role_policy) : 0

  statement {
    effect = lookup(
      var.assume_role_policy[count.index],
      "statement_efect",
      "Deny",
    )
    actions = split(
      ",",
      var.assume_role_policy[count.index]["statement_actions"],
    )
    resources = split(
      ",",
      var.assume_role_policy[count.index]["statement_resources"],
    )
  }
}

data "aws_iam_policy_document" "assume_role_policy_condition" {
  count = var.create_assume_role ? length(var.assume_role_policy_condition) : 0

  statement {
    effect = lookup(
      var.assume_role_policy_condition[count.index],
      "statement_efect",
      "Deny",
    )
    actions = split(
      ",",
      var.assume_role_policy_condition[count.index]["statement_actions"],
    )
    resources = split(
      ",",
      var.assume_role_policy_condition[count.index]["statement_resources"],
    )

    condition {
      test     = var.assume_role_policy_condition[count.index]["condition_test"]
      variable = var.assume_role_policy_condition[count.index]["condition_variable"]
      values = split(
        ",",
        var.assume_role_policy_condition[count.index]["condition_values"],
      )
    }
  }
}

resource "aws_iam_role_policy" "assume_role_policy" {
  count = var.create_assume_role ? length(var.assume_role_policy) : 0

  name_prefix = format("%s-", var.name)
  role        = element(concat(aws_iam_role.assume_role.*.id, [""]), 0)

  policy = element(
    data.aws_iam_policy_document.assume_role_policy.*.json,
    count.index,
  )
}

resource "aws_iam_role_policy" "assume_role_policy_condition" {
  count = var.create_assume_role ? length(var.assume_role_policy_condition) : 0

  name_prefix = format("%s-", var.name)
  role        = element(concat(aws_iam_role.assume_role.*.id, [""]), 0)

  policy = element(
    data.aws_iam_policy_document.assume_role_policy_condition.*.json,
    count.index,
  )
}

resource "aws_iam_instance_profile" "assume_role_instance_profile" {
  count = var.create_assume_role ? 1 : 0

  name_prefix = format("%s-", var.name)
  role        = element(concat(aws_iam_role.assume_role.*.id, [""]), 0)
}

